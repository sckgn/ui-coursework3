//
// Created by twak on 11/11/2019.
//

#include "the_player.h"

using namespace std;

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i, vector<QSlider*> p, vector<QLabel*> l, vector<QPushButton*> m) {
    buttons = b;
    infos = i;
    bars = p;
    labels = l;
    controls = m;

    //duration_label = l.at(0);
    bars.at(0)->setOrientation(Qt::Horizontal);
    bars.at(0)->setFixedHeight(20);
    jumpTo(buttons -> at(0) -> info);
}

// change the image and video for one button every one second
/*void ThePlayer::shuffle() {
    TheButtonInfo* i = & infos -> at (rand() % infos->size() );
//        setMedia(*i->url);
    buttons -> at( updateCount++ % buttons->size() ) -> init( i );
}*/

void ThePlayer::next(){
    for (int i = 0; i < (int) buttons->size(); i++){
        if (buttons->at(i)->info->url == current->url && (i + 1) < (int) buttons->size()){
            setMedia(*buttons->at(i + 1)->info->url);
            current = buttons->at(i + 1)->info;
            play();
            break;
        }
    }
}


void ThePlayer::previous(){
    for (int i = 0; i < (int) infos->size(); i++){
        if (infos->at(i).url == current->url && i > 0){
            setMedia(*buttons->at(i - 1)->info->url);
            current = buttons->at(i - 1)->info;
            play();
            break;
        }
    }
}

void ThePlayer::refresh_duration_bar(){
    bars.at(0)->setValue(position());
    bars.at(0)->setRange(0, duration());
    char temp[24];
    string duration_text;
    sprintf(temp, "%.2f", (float) position()/1000);
    duration_text = temp;
    duration_text = duration_text + '/';
    sprintf(temp, "%.2f", (float) duration()/1000);
    duration_text = duration_text + temp;
    labels.at(0)->setText(QString::fromStdString(duration_text));

}

void ThePlayer::change_volume(){
    setVolume(bars.at(1)->value());
}

void ThePlayer::mute(){
    if (bars.at(1)->value() == 0){
        controls.at(0)->setIcon(QIcon("C:/Users/Middl/Downloads/ui-coursework3-cycle2/ui-coursework3-cycle2/icons/Button-Sound-icon.png"));
        bars.at(1)->setValue(volume_tracker);
    }
    else{
        controls.at(0)->setIcon(QIcon("C:/Users/Middl/Downloads/ui-coursework3-cycle2/ui-coursework3-cycle2/icons/Button-Mute-icon.png"));
        volume_tracker = bars.at(1)->value();
        bars.at(1)->setValue(0);
    }

}

void ThePlayer::upload(){
    QWidget *upload_window = new QWidget();
    upload_window->setWindowTitle("Upload Video to Tomeo");
    upload_window->setMinimumSize(QSize(300,100));
    QVBoxLayout *upload_window_layout = new QVBoxLayout();
    upload_window->setLayout(upload_window_layout);
    QString upload_address;
    upload_address  = QInputDialog::getText(upload_window, "Upload Video","Input Video Address");
    append_video(upload_address);
    upload_window->show();
}

void ThePlayer::append_video(QString v){
    address->fromLocalFile(v);
    icon->addFile("C:/Users/Middl/Downloads/ui-coursework3-cycle2/ui-coursework3-cycle2/icons/template_thumbnail");
    TheButtonInfo(address,icon);
}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            play(); // starting playing again...
            break;
    default:
        break;
    }
}

void ThePlayer::jumpTo (TheButtonInfo* button) {
    setMedia( * button -> url);
    current = button;
    play();
}
