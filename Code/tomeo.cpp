/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for sports enthusiasts...
 *
 *  2811 cw3 : twak
 */

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <string>
#include <vector>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QLabel>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QDesktopServices>
#include <QImageReader>
#include <QMessageBox>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include "the_player.h"
#include "the_button.h"


using namespace std;

// read in videos and thumbnails to this directory
vector<TheButtonInfo> getInfoIn (string loc) {

    vector<TheButtonInfo> out =  vector<TheButtonInfo>();
    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files

        QString f = it.next();

            if (f.contains("."))

#if defined(_WIN32)
            if (f.contains(".wmv"))  { // windows
#else
            if (f.contains(".mp4") || f.contains("MOV"))  { // mac/linux
#endif

            QString thumb = f.left( f .length() - 4) +".png";
            if (QFile(thumb).exists()) { // if a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                    QImage sprite = imageReader->read(); // read the thumbnail
                    if (!sprite.isNull()) {
                        QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                        QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                        out . push_back(TheButtonInfo( url , ico  ) ); // add to the output list
                    }
                    else
                        qDebug() << "warning: skipping video because I couldn't process thumbnail " << thumb << Qt::endl;
            }
            else
                qDebug() << "warning: skipping video because I couldn't find thumbnail " << thumb << Qt::endl;
        }
    }

    return out;
}


int main(int argc, char *argv[]) {

    // let's just check that Qt is operational first
    qDebug() << "Qt version: " << QT_VERSION_STR << Qt::endl;

    // create the Qt Application
    QApplication app(argc, argv);

    // collect all the videos in the folder
    vector<TheButtonInfo> videos;

    if (argc == 2)
        videos = getInfoIn( string(argv[1]) );

    if (videos.size() == 0) {

        const int result = QMessageBox::question(
                    NULL,
                    QString("Tomeo"),
                    QString("no videos found! download, unzip, and add command line argument to \"quoted\" file location. Download videos from Tom's OneDrive?"),
                    QMessageBox::Yes |
                    QMessageBox::No );

        switch( result )
        {
        case QMessageBox::Yes:
          QDesktopServices::openUrl(QUrl("https://leeds365-my.sharepoint.com/:u:/g/personal/scstke_leeds_ac_uk/EcGntcL-K3JOiaZF4T_uaA4BHn6USbq2E55kF_BTfdpPag?e=n1qfuN"));
          break;
        default:
            break;
        }
        exit(-1);
    }

    // the widget that will show the video
    QVideoWidget *videoWidget = new QVideoWidget;

    // the QMediaPlayer which controls the playback
    ThePlayer *player = new ThePlayer;
    player->setVideoOutput(videoWidget);
    // Making progress Bar
    // a row of buttons
    // a list of the buttons
    vector<TheButton*> buttons;
    // list of labels for buttons
    vector<QLabel*> labels;
    vector<QSlider*> sliders;
    vector<QPushButton*> control_buttons;

    // Adding elements to the layout
    //Layouts
    QVBoxLayout *layout = new QVBoxLayout();
    QWidget *layout_widget = new QWidget;
    //Creates row containing duration bar and current time
    QHBoxLayout *media_layout = new QHBoxLayout();
    QWidget *media_row = new QWidget();
    media_row->setLayout(media_layout);
    layout->addWidget(media_row);
    //Creates row of controls
    QHBoxLayout *control_layout = new QHBoxLayout();
    QWidget *control_widget = new QWidget();
    control_widget->setLayout(control_layout);
    layout->addWidget(control_widget);
    //Creats row of video buttons
    QHBoxLayout *buttons_layout = new QHBoxLayout();
    QWidget *buttonWidget = new QWidget();
    buttonWidget->setLayout(buttons_layout);
    layout->addWidget(buttonWidget);
    layout_widget->setLayout(layout);


    QSlider *progress_bar = new QSlider();
    sliders.push_back(progress_bar);
    media_layout->addWidget(progress_bar);
    QLabel *duration_label = new QLabel();
    media_layout->addWidget(duration_label);
    labels.push_back(duration_label);
    QWidget *buttons_widget = new QWidget();
    buttons_widget->setLayout(buttons_layout);
    layout->addWidget(buttons_widget);

    QPushButton *mute_button = new QPushButton;
    mute_button->setFixedWidth(75);
    mute_button->setIcon(QIcon("C:/Users/Middl/Downloads/ui-coursework3-cycle2/ui-coursework3-cycle2/icons/Button-Sound-icon.png"));
    mute_button->setIconSize(QSize(65,65));
    control_layout->addWidget(mute_button);
    mute_button->connect(mute_button, SIGNAL(clicked()), player, SLOT(mute()));
    control_buttons.push_back(mute_button);

    QSlider *volume_slider = new QSlider;
    sliders.push_back(volume_slider);
    volume_slider->setOrientation(Qt::Horizontal);
    volume_slider->setFixedWidth(200);
    volume_slider->setRange(0, 100);
    volume_slider->setValue(50);
    control_layout->addWidget(volume_slider);


    QPushButton *previous_button = new QPushButton;
    previous_button->setIcon(QIcon("C:/Users/Middl/Downloads/ui-coursework3-cycle2/ui-coursework3-cycle2/icons/Button-Skip-Backwards-icon.png"));
    previous_button->setIconSize(QSize(65,65));
    previous_button->setFixedWidth(75);
    previous_button->connect(previous_button, SIGNAL(clicked()), player, SLOT(previous()));
    control_layout->addWidget(previous_button);
    control_buttons.push_back(previous_button);

    //Have to manually change the address of buttons to correct one
    QPushButton *play_button = new QPushButton;
    play_button->setIcon(QIcon("C:/Users/Middl/Downloads/ui-coursework3-cycle2/ui-coursework3-cycle2/icons/Button-Play-icon.png"));
    play_button->setIconSize(QSize(65,65));
    play_button->setFixedWidth(75);
    play_button->connect(play_button, SIGNAL(clicked()), player, SLOT(play()));
    control_layout->addWidget(play_button);
    control_buttons.push_back(play_button);

    QPushButton *pause_button = new QPushButton;
    pause_button->connect(pause_button, SIGNAL(clicked()), player, SLOT(pause()));
    pause_button->setIcon(QIcon("C:/Users/Middl/Downloads/ui-coursework3-cycle2/ui-coursework3-cycle2/icons/Button-Pause-icon.png"));
    pause_button->setIconSize(QSize(65,65));
    pause_button->setFixedWidth(75);
    control_layout->addWidget(pause_button);
    control_buttons.push_back(pause_button);

    QPushButton *next_button = new QPushButton;
    next_button->setIcon(QIcon("C:/Users/Middl/Downloads/ui-coursework3-cycle2/ui-coursework3-cycle2/icons/Button-Skip-Forward-icon.png"));
    next_button->setIconSize(QSize(65,65));
    next_button->setFixedWidth(75);
    next_button->connect(next_button, SIGNAL(clicked()), player, SLOT(next()));
    control_layout->addWidget(next_button);
    control_buttons.push_back(next_button);

    QPushButton *upload_button = new QPushButton;
    upload_button->setIcon(QIcon("C:/Users/Middl/Downloads/ui-coursework3-cycle2/ui-coursework3-cycle2/icons/Button-Upload-icon.png"));
    upload_button->setIconSize(QSize(65,65));
    upload_button->setFixedWidth(75);
    upload_button->connect(upload_button, SIGNAL(clicked()), player, SLOT(upload()));
    control_layout->addWidget(upload_button);
    control_buttons.push_back(upload_button);

    // create the buttons
    for (int i = 0; i < (int) videos.size(); i++){
        TheButton *button = new TheButton(buttonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        buttons.push_back(button);
        buttons_layout->addWidget(button);
        button->init(&videos.at(i));
    }

    // tell the player what buttons and videos are available
    player->setContent(&buttons, & videos, sliders, labels, control_buttons);

    // create the main window and layout
    QWidget window;
    QVBoxLayout *top = new QVBoxLayout();

    window.setLayout(top);
    window.setWindowTitle("tomeo");
    window.setMinimumSize(800, 680);

    // add the video and the buttons to the top level widget
    top->addWidget(videoWidget);
    top->addWidget(layout_widget);

    // showtime!
    window.show();

    // wait for the app to terminate
    return app.exec();
}
