//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_BUTTON_H
#define CW2_THE_BUTTON_H


#include <QPushButton>
#include <QMouseEvent>
#include <QProgressBar>
#include <QLabel>
#include <QSlider>
#include <QUrl>


class TheButtonInfo {

public:
    QUrl* url; // video file to play
    QIcon* icon; // icon to display

    TheButtonInfo ( QUrl* url, QIcon* icon) : url (url), icon (icon) {}
};

class TheButton : public QPushButton {
    Q_OBJECT

public:
    TheButtonInfo* info;

     TheButton(QWidget *parent) :  QPushButton(parent) {
         setIconSize(QSize(200,110));
         connect(this, SIGNAL(released()), this, SLOT (clicked() )); // if QPushButton clicked...then run clicked() below
    }

    void init(TheButtonInfo* i);

private slots:
    void clicked();


signals:
    void jumpTo(TheButtonInfo*);

};

/*class TheProgressBar : public QProgressBar {
    Q_OBJECT

public:
    TheProgressBar(QWidget *parent) : QProgressBar(parent) {
        setFixedHeight(100);
    }
};*/

class TheProgressBar : public QSlider {
    Q_OBJECT

public:
    TheProgressBar(QWidget * parent) : QSlider(parent) {
        setFixedHeight(100);
    }
};

#endif //CW2_THE_BUTTON_H
