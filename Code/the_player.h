//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_PLAYER_H
#define CW2_THE_PLAYER_H


#include <QApplication>
#include <QMediaPlayer>
#include <QVBoxLayout>
#include <QInputDialog>
#include <math.h>
#include <QLabel>
#include "the_button.h"
#include <vector>
#include <QTimer>

using namespace std;

class ThePlayer : public QMediaPlayer {

Q_OBJECT

private:
    vector<TheButtonInfo>* infos;
    vector<TheButton*>* buttons;
    vector<QLabel*> labels;
    vector<QPushButton*> controls;
    vector<QSlider*> bars;
    TheButtonInfo* current;
    QTimer* mTimer;
    QPushButton* mute_button;
    QUrl *address;
    QIcon *icon;
    int volume_tracker = 50;
    long updateCount = 0;

public:
    ThePlayer() : QMediaPlayer(NULL) {
        connect (this, SIGNAL (stateChanged(QMediaPlayer::State)), this, SLOT (playStateChanged(QMediaPlayer::State)) );
        mTimer = new QTimer(NULL);
        mTimer->setInterval(100); // 1000ms is one second between ...
        mTimer->start();
        connect( mTimer, SIGNAL(timeout()), SLOT(refresh_duration_bar()));
        connect(mTimer, SIGNAL(timeout()), SLOT(change_volume()));
    }

    // all buttons have been setup, store pointers here
    void setContent(vector<TheButton*>* b, vector<TheButtonInfo>* i, vector<QSlider*> p, vector<QLabel*> l, vector<QPushButton*> m);

private slots:

    // change the image and video for one button every one second
    //void shuffle();

    void playStateChanged (QMediaPlayer::State ms);

    void next();

    void previous();

    void refresh_duration_bar();

    void change_volume();

    void mute();

    void upload();

    void append_video(QString v);

public slots:

    // start playing this ButtonInfo
    void jumpTo (TheButtonInfo* button);

};

#endif //CW2_THE_PLAYER_H
